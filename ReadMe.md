# ElevatorRemoteControl application code
Remote control of the elevator via Bluetooth.

Tested with Android 6.0.

Use https://github.com/harry1453/android-bluetooth-serial for serial Bluetooth communication.