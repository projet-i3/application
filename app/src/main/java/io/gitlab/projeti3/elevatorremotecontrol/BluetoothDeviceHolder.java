package io.gitlab.projeti3.elevatorremotecontrol;

import android.bluetooth.BluetoothDevice;

import androidx.annotation.NonNull;

/**
 * This class is a workaround because we can't override the toString method of BluetoothDevice.
 */
public class BluetoothDeviceHolder {
    private final BluetoothDevice bluetoothDevice;

    /**
     * Create a new BluetoothDeviceHolder.
     *
     * @param bluetoothDevice The bluetooth device.
     */
    public BluetoothDeviceHolder(BluetoothDevice bluetoothDevice) {
        this.bluetoothDevice = bluetoothDevice;
    }

    /**
     * toString method with name and address of the device.
     *
     * @return "nameOfTheDevice (MacAddressOfTheDevice)"
     */
    @Override
    @NonNull
    public String toString() {
        return bluetoothDevice.getName() + " (" + bluetoothDevice.getAddress() + ")";
    }

    /**
     * Get the original BluetoothDevice.
     *
     * @return The bluetoothDevice.
     */
    public BluetoothDevice getBluetoothDevice() {
        return bluetoothDevice;
    }
}
