package io.gitlab.projeti3.elevatorremotecontrol;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.harrysoft.androidbluetoothserial.BluetoothManager;
import com.harrysoft.androidbluetoothserial.BluetoothSerialDevice;
import com.harrysoft.androidbluetoothserial.SimpleBluetoothDeviceInterface;

import java.util.ArrayList;
import java.util.List;

import io.gitlab.projeti3.elevatorremotecontrol.databinding.ActivityMainBinding;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = MainActivity.class.getSimpleName();

    private ActivityMainBinding binding;

    private SimpleBluetoothDeviceInterface deviceInterface;
    private BluetoothManager bluetoothManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View root = binding.getRoot();
        setContentView(root);
        root.setOnClickListener(this);
        for (View view : binding.getRoot().getTouchables()) {
            if (view instanceof Button) {
                view.setOnClickListener(this);
            }
        }
        bluetoothManager = BluetoothManager.getInstance();
        if (bluetoothManager == null) {
            Toast.makeText(getBaseContext(), R.string.no_bluetooth, Toast.LENGTH_LONG).show();
            finish();
        } else {
            if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                Toast.makeText(getBaseContext(), R.string.bluetooth_disable, Toast.LENGTH_LONG).show();
                finish();
            }
            List<BluetoothDevice> pairedDevices = bluetoothManager.getPairedDevicesList();
            List<BluetoothDeviceHolder> bluetoothDeviceHolders = new ArrayList<>();
            for (BluetoothDevice device : pairedDevices) {
                bluetoothDeviceHolders.add(new BluetoothDeviceHolder(device));
            }
            ArrayAdapter<BluetoothDeviceHolder> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, bluetoothDeviceHolders);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            binding.spDevices.setAdapter(adapter);
        }
    }

    /**
     * Connect to device with mac address.
     *
     * @param mac The mac address of the device to connect.
     */
    private void connectDevice(String mac) {
        Disposable subscribe = bluetoothManager.openSerialDevice(mac)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onConnected, this::onError);
    }

    /**
     * Run once the device is connected.
     *
     * @param connectedDevice The connected device.
     */
    private void onConnected(BluetoothSerialDevice connectedDevice) {
        deviceInterface = connectedDevice.toSimpleDeviceInterface();

        deviceInterface.setListeners(this::onMessageReceived, null, this::onError);
    }

    /**
     * Run when a new message is received.
     *
     * @param message The message received.
     */
    private void onMessageReceived(String message) {
        // This Regex prevent from having an empty "" for the first value (Java 7).
        String[] values = message.split("(?!^)");
        Log.d(TAG, "message: " + message);

        if (Integer.parseInt(values[0]) == 1) {
            binding.btnExt0.setBackgroundColor(ContextCompat.getColor(this, R.color.remoteSelected));
        } else {
            binding.btnExt0.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
        if (Integer.parseInt(values[1]) == 1) {
            binding.btnExt1.setBackgroundColor(ContextCompat.getColor(this, R.color.remoteSelected));
        } else {
            binding.btnExt1.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
        if (Integer.parseInt(values[2]) == 1) {
            binding.btnExt2.setBackgroundColor(ContextCompat.getColor(this, R.color.remoteSelected));
        } else {
            binding.btnExt2.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
        if (Integer.parseInt(values[3]) == 1) {
            binding.btnExt3.setBackgroundColor(ContextCompat.getColor(this, R.color.remoteSelected));
        } else {
            binding.btnExt3.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
        if (Integer.parseInt(values[4]) == 1) {
            binding.btnInt0.setBackgroundColor(ContextCompat.getColor(this, R.color.remoteSelected));
        } else {
            binding.btnInt0.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
        if (Integer.parseInt(values[5]) == 1) {
            binding.btnInt1.setBackgroundColor(ContextCompat.getColor(this, R.color.remoteSelected));
        } else {
            binding.btnInt1.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
        if (Integer.parseInt(values[6]) == 1) {
            binding.btnInt2.setBackgroundColor(ContextCompat.getColor(this, R.color.remoteSelected));
        } else {
            binding.btnInt2.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
        if (Integer.parseInt(values[7]) == 1) {
            binding.btnInt3.setBackgroundColor(ContextCompat.getColor(this, R.color.remoteSelected));
        } else {
            binding.btnInt3.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
        if (Integer.parseInt(values[8]) == 1) {
            binding.btnDoor.setBackgroundColor(ContextCompat.getColor(this, R.color.remoteSelected));
        } else {
            binding.btnDoor.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
        if (Integer.parseInt(values[9]) == 1) {
            binding.tvObstacle.setText(R.string.no_obstacle);
        } else {
            binding.tvObstacle.setText(R.string.obstacle);
        }
        if (Integer.parseInt(values[10]) == 1) {
            if (Integer.parseInt(values[11]) == 1) {
                binding.tvDoor.setText(R.string.door_closed);
            } else {
                binding.tvDoor.setText(R.string.door_moving);
            }
        } else {
            binding.tvDoor.setText(R.string.door_open);
        }
        binding.tvCurrentFloor.setText(values[12]);
        if (values[13].equals("U")) {
            binding.tvDestinationFloor.setText(R.string.no_destination);
        } else {
            binding.tvDestinationFloor.setText(values[13]);
        }
        int movingState = Integer.parseInt(values[14]);
        switch (movingState) {
            case 0:
                binding.tvDirection.setText(R.string.direction_idle);
                break;
            case 1:
                binding.tvDirection.setText(R.string.direction_up);
                break;
            case 2:
                binding.tvDirection.setText(R.string.direction_down);
                break;
        }
    }

    /**
     * Run when there is an error.
     *
     * @param error The error.
     */
    private void onError(Throwable error) {
        Log.w(TAG, "onError: " + error);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == binding.btnExt0.getId()) {
            deviceInterface.sendMessage("E0\r\n");
        } else if (id == binding.btnExt1.getId()) {
            deviceInterface.sendMessage("E1\r\n");
        } else if (id == binding.btnExt2.getId()) {
            deviceInterface.sendMessage("E2\r\n");
        } else if (id == binding.btnExt3.getId()) {
            deviceInterface.sendMessage("E3\r\n");
        } else if (id == binding.btnInt0.getId()) {
            deviceInterface.sendMessage("I0\r\n");
        } else if (id == binding.btnInt1.getId()) {
            deviceInterface.sendMessage("I1\r\n");
        } else if (id == binding.btnInt2.getId()) {
            deviceInterface.sendMessage("I2\r\n");
        } else if (id == binding.btnInt3.getId()) {
            deviceInterface.sendMessage("I3\r\n");
        } else if (id == binding.btnDoor.getId()) {
            deviceInterface.sendMessage("D\r\n");
        } else if (id == binding.btnConnect.getId()) {
            connectDevice(((BluetoothDeviceHolder) binding.spDevices.getSelectedItem()).getBluetoothDevice().getAddress());
        }
    }
}